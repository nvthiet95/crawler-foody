<?php
if(isset($_GET['download'])) {
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=data.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
}
?>
<?php
$allData = json_decode(@file_get_contents('data.txt'), true);
$allData = !empty($allData) ? $allData : [];
$allData = array_reverse($allData);
?>
<meta charset="utf-8" />
<?php if(!isset($_GET['download'])) { ?>
<a href="?download=true">DOWNLOAD</a>
<?php } ?>
<br /><br />
<table border="1" style="border-collapse: collapse;">
    <thead>
        <tr>
            <th>STT</th>
            <th>Link Foody</th>
            <th>Tiêu Đề</th>
            <th>Địa Chỉ</th>
            <th>Thời Gian</th>
            <th>SĐT</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($allData as $k => $row):?>
        <tr>
            <td style="text-align:center"><?php echo (count($allData)-$k) ?></td>
            <td><?php echo $row['foody_link']?></td>
            <td><?php echo $row['tieu_de']?></td>
            <td><?php echo $row['dia_chi']?></td>
            <td><?php echo $row['thoi_gian']?></td>
            <td><?php echo empty($row['sdt']) ? 'Đang cập nhật...' : implode(',',$row['sdt']) ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>   